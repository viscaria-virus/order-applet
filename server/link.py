import socketio,sys,json

# 导入监听端口配置
with open('static/config.json','r') as f:
    port=json.load(f)['port']['websocket']
# 生成WebSocket服务器URL
url=f'ws://localhost:{port}'
# 创建WebSocket客户端
sender=socketio.Client()
# 连接WebSocket服务端
sender.connect(url)
# 从命令行接收消息参数
message=sys.argv[1]
# 将消息发送到WebSocket服务端
sender.send(message)
# 关闭WebSocket连接
sender.disconnect()
