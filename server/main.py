import os
from threading import Thread

def serve(service):
    os.system(f'python {service}.py')

t1=Thread(target=lambda:serve('ws_server'))
t2=Thread(target=lambda:serve('http_server'))
t1.start()
t2.start()
