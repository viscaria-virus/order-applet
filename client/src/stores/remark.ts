import {defineStore} from 'pinia';
import {readonly, ref} from 'vue';

// 定义全局状态-备注
export const useRemarkStore=defineStore('remark',()=>{
    //初始化备注内容
    const content=ref<string>('');
    //更新备注方法
    function update(newContent:string):void{
        content.value=newContent;
    }
    //清空备注方法
    function clear():void{
        update('');
    }
    return {
        content:readonly(content),
        update,
        clear
    };
});
