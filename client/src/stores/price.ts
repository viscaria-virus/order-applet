import {defineStore} from 'pinia';
import {readonly, ref} from 'vue';

//定义全局状态-总价
export const usePriceStore=defineStore('price',()=>{
    //初始化总价值
    const value=ref<number>(0);
    //增加总价方法
    function add(price:number):void{
        value.value+=price;
    };
    //重置总价方法
    function reset():void{
        value.value=0;
    }
    return {
        value:readonly(value),
        add,
        reset
    };
});
