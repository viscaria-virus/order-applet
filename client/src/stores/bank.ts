import axios from 'axios';
import {defineStore} from 'pinia';
import {readonly, ref} from 'vue';
//导入全局状态-配置
import {useConfigStore} from "./config";

//定义库存信息类型
export interface BankInfo{
    [index:string]:boolean
}
//定义全局状态-库存信息
export const useBankStore=defineStore('bank',()=>{
    //初始化库存状态
    const detail=ref<BankInfo>({});
    //使用全局状态-配置
    const config=useConfigStore();
    //请求服务端获取库存信息
    axios.get(config.database)
        .then((response)=>{
            // 加载库存数据
            detail.value=response.data;
        })
        .catch((error)=>{
            // 弹出报错信息
            alert(error.message);
        });
    //更新库存信息方法
    function update(data:BankInfo):void{
        Object.assign(detail.value,JSON.parse(JSON.stringify(data)));
    }
    return {
        detail:readonly(detail),
        update
    };
});
