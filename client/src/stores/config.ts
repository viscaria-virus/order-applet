import {defineStore} from 'pinia';
// 导入配置文件
import {server} from '../static/config.json';

//定义全局状态-配置信息
export const useConfigStore=defineStore('config',()=>{
    //服务端地址
    const url:string=server.url;
    //主页地址
    const home:string=[url,server.home].join('/');
    //数据库地址
    const database:string=[url,server.database].join('/');
    return {home,database};
});
