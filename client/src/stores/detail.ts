import { readonly, ref, type Ref } from "vue";
import { defineStore,storeToRefs } from "pinia";
// 导入全局状态-购物清单、总价、备注
import { useListStore, type ShoppingList } from "./list";
import { usePriceStore } from "./price";
import { useRemarkStore } from "./remark";

//定义订单信息内容类型
export interface OrderInfoContent{
    table:number,
    list:Ref<ShoppingList>,
    price:Ref<number>,
    remark:Ref<string>,
    time:Ref<string>
}
//定义全局状态-订单信息
export const useDetailStore=defineStore('order',()=>{
    //使用导入的全局状态
    const [list,price,remark]=[useListStore(),usePriceStore(),useRemarkStore()];
    //初始化订单编号
    const id=ref<string|number>('<待获取>');
    //初始化下单时间
    const time=ref<string>('');
    //从URL路径中读取取桌号
    const table:number=Number(location.pathname.slice(1));
    //生成订单信息内容
    const content=ref<OrderInfoContent>({
        table,
        list:storeToRefs(list).detail,
        price:storeToRefs(price).value,
        remark:storeToRefs(remark).content,
        time
    });
    //重置订单信息方法
    function reset():void{
        list.clear();
        price.reset();
        remark.clear();
        time.value='';
        id.value='<待获取>';
    }
    //设置订单编号方法
    function useID(orderID:number):void{
        id.value=orderID;
    }
    //获取下单时间方法
    function getTime():void{
        const date=new Date();
        const dateString=date.toJSON().split('T')[0];
        const timeString=date.toLocaleTimeString();
        time.value=`[${dateString}][${timeString}]`;
    }
    return {
        content:readonly(content),
        id:readonly(id),
        time:readonly(time),
        reset,
        useID,
        getTime
    };
});
