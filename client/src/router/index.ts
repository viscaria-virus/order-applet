const MenuPage=()=>import('../pages/menu.vue');
const ConfirmPage=()=>import('../pages/confirm.vue');
const FinishPage=()=>import('../pages/finish.vue');
const DetailPage=()=>import('../pages/detail.vue');

export const routes=[
    {
        // 菜单列表页面
        path:'/',
        component:MenuPage
    },
    {
        // 确认订单页面   
        path:'/confirm',
        component:ConfirmPage,
        children:[
            {
                // 订单信息
                path:'detail',
                component:DetailPage
            },
            {
                // 下单反馈
                path:'finish/:success',
                component:FinishPage
            }
        ]
    }
];
