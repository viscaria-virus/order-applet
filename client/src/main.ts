import { createSSRApp } from "vue";
import { createPinia } from "pinia";
import { createRouter, createWebHashHistory } from "vue-router";
import App from "@/App.vue";
import { routes } from "@/router";
export function createApp() {
  const app=createSSRApp(App);
  const pinia=createPinia();
  app.use(pinia);
  const router=createRouter({
    history:createWebHashHistory(),
    routes
  })
  app.use(router);
  return {
    app
  };
}
