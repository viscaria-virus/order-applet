# 点餐小程序（新龙记鸡公煲）

## 简介

为新龙记鸡公煲华中农业大学梧桐路步行街加盟店开发的扫码点餐小程序。

## 详情

### 系统架构

+ 服务端部署后通过HTTP服务提供客户端的访问。
+ 消费者通过手机扫码访问服务端返回的客户端页面，客户端页面向服务端发送异步请求拉取库存信息并更新页面渲染，用户提交订单时向服务端发送请求核验库存状态并返回下单结果。
+ 商家桌面端与服务端建立WebSocket连接以实时同步订单信息和库存信息。
+ 商家移动端通过HTTP请求向服务端发送指令来修改库存状态。

### 客户端

+ 基于Vue3+uni-app+TypeScript深度重构项目,广泛采用Vue框架的各种功能与特性。
+ 采用SCSS重构了样式代码，响应式单位由rem/%改为vw/vh/%实现。
+ 仅采用了框架内置组件，未使用额外组件库，自行封装了部分组件。
+ 使用pinia进行状态管理，接收并缓存库存信息、存储订单状态、加载和更新用户历史订单信息；外部访问全局状态暴露的属性时只允许通过其暴露的方法来修改值。
+ 使用vue-router进行路由管理，基于嵌套路由实现下单页反馈信息跳转，采用路由懒加载改善用户体验。
+ 使用axios库进行AJAX请求以便于维护和兼顾安全性。
+ 从服务端数据库获取库存信息后缓存为响应式全局状态供组件访问以实现条件渲染。
+ 发送订单时接收服务端反馈的库存状态校验信息。
+ 基于浏览器本地缓存构建了订单信息缓存和历史订单页面。
+ 提供拉起支付宝扫码功能，供消费者扫商家收款码完成支付。
+ 备注事项(uni-app)

> 1. 为了解决自定义组件样式失效问题，设置了全局样式并将类名作为属性透传给自定义组件TextButton内的button组件。
> 2. 为了解决自定义组件点击事件失效问题，在自定义组件IconButton和TextButton中设置button组件的点击事件回调为分发一个click事件供父组件监听。
> 3. 为了避免打包后导入失效问题，所有导入路径均采用相对路径。

#### 项目结构

```shell
client
├── index.html 入口页面
├── package.json 项目依赖配置
├── src
│   ├── App.vue 应用入口组件
│   ├── components
│   │   ├── Bar.vue 底部导航条组件
│   │   ├── Drinks.vue 饮料列表组件
│   │   ├── History.vue 历史订单列表组件
│   │   ├── MainDishes.vue 煲类菜单列表组件
│   │   ├── SideDishes.vue 加菜菜单列表组件
│   │   ├── Tab.vue 侧边导航栏组件
│   │   └── common
│   │       ├── Dish.vue 菜品列表块组件
│   │       ├── IconButton.vue 圆形图标按钮组件
│   │       ├── Popup.vue 弹出层组件
│   │       └── TextButton.vue 文本按钮组件
│   ├── env.d.ts 类型声明
│   ├── main.ts 应用入口脚本
│   ├── manifest.json 应用配置
│   ├── pages
│   │   ├── confirm.vue 下单确认页面
│   │   ├── detail.vue 订单详情页面
│   │   ├── finish.vue 下单反馈页面
│   │   ├── index.vue 主页面
│   │   └── menu.vue 菜单页面
│   ├── pages.json 页面路由配置(uni-app)
│   ├── router
│   │   └── index.ts 页面路由配置(vue-router)
│   ├── static
│   │   ├── config.json 服务端配置
│   │   ├── cup.png 饮料图片
│   │   ├── dish.png 配菜图片
│   │   ├── menu.json 菜单配置
│   │   └── stew.png 主菜图片
│   ├── stores
│   │   ├── bank.ts 菜品库存状态
│   │   ├── cache.ts 订单缓存状态
│   │   ├── config.ts 全局配置状态
│   │   ├── detail.ts 订单详情状态
│   │   ├── list.ts 购物清单状态
│   │   ├── price.ts 购物总价状态
│   │   └── remark.ts 购物备注状态
│   ├── styles
│   │   ├── shoppingListStyle.scss 购物清单列表样式
│   │   └── textLayout.scss 文本布局样式
│   └── uni.scss 全局内置样式(uni-app)
├── tips.html 非手机端访问跳转提示页面
├── tsconfig.json 编译配置(TypeScript)
└── vite.config.ts 编译配置(Vite)
```

### 服务端

+ 采用flask框架构建，采用Nuitka编译打包。
+ 客户端打包成静态资源引用。
+ 基于HTTP服务实现接收订单和校验库存功能。
+ 基于动态路由实现桌号匹配功能。
+ 基于WebSocket实现服务端转发下单信息给商家桌面端功能。

#### 项目结构

```shell
server
├── http_server.py 服务端脚本(HTTP)
├── link.py 通信脚本(HTTP-WebSocket)
├── main.py 应用入口脚本
├── pyvenv.cfg 虚拟环境配置文件
├── requirements.txt 项目依赖配置
├── static
│   ├── bank.json 初始库存信息
│   └── config.json 服务监听端口配置
├── test.html 商家桌面端模拟页面
└── ws_server.py 服务端脚本(WebSocket)
```
